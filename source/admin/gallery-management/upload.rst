Uploading Galleries
===================
#. Login as Photographer or Administrator. If you don't have these permissions get them from current admins (see :doc:`/admin/getting-started/access-credentials`).
#. Click on Upload on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`).
#. Drag and drop images or click on the "Select Images to Upload" button.
#. Update the gallery information on the right side (see :ref:`update-gallery-information`).
#. The gallery, by default, has private permissions (see :doc:`private`). To let everyone see it on the site, check "Make this gallery public" box.
#. Once you are happy with the details, "Publish" or "Save as Draft" if you feel you missed something and want to publish it in a later time. For managing draft galleries, please see :doc:`drafts`.

.. image:: /_static/admin/gallery-management/save.png


.. _update-gallery-information:

Updating Gallery Information
--------------------------------
1. Click on the data you want to change.

.. image:: /_static/admin/gallery-management/upload-edit-info1.png

2. Confirm or cancel changes.

.. image:: /_static/admin/gallery-management/upload-edit-info2.png
