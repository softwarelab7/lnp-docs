Draft Galleries
==================

.. _finding-drafts:

Finding Draft Galleries
-----------------------
#. Login to your account.
#. Find gallery.
    - For Photographers
        #. Click on Galleries > My Galleries > Drafts on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`). This should take you to the page that lists all your draft uploads so that you can edit them.
        #. Search for your gallery (see :ref:`gallery-select`).

    - For Admin
        #. Click on Manage > Galleries on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`). This should take you to administration pages for the galleries.
        #. Filter with Status: Draft (see :ref:`gallery-admin-search-and-filter`).


Updating Draft Galleries
---------------------------------------

For Photographers
~~~~~~~~~~~~~~~~~~~
#. :ref:`finding-drafts`.
#. Select gallery to edit (see :ref:`gallery-select`).
#. Click on the pencil icon (|pencil-icon|).
#. The controls should be similar to :doc:`/admin/gallery-management/upload`.

For Admin
~~~~~~~~~~~~~~~~~~~
#. :ref:`finding-drafts`.
#. Click on the pencil icon (|pencil-icon|).
#. The controls should be similar to :doc:`/admin/gallery-management/upload`.

Deleting Drafts
---------------
#. :ref:`finding-drafts`.
#. Select gallery to edit (see :ref:`gallery-select`).
#. Click on the trashcan icon (|trash-icon|).
#. Confirm cancellation by clicking the "Delete" button.
#. You should be prompted that the gallery was deleted.
