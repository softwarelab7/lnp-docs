Private Galleries
=========================
These are galleries that only people with the access code can access apart from the admins and photographer who uploaded the gallery.

Sharing Gallery Access
--------------------------------
#. Login to your account.
#. Selecting a gallery.
    - For Photographers:
        #. Click on Galleries > My Galleries > Published on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`). This should take you to the page that lists all your published galleries. |permissions|
        #. Select gallery to edit (see :ref:`gallery-select`).
        #. Click on the invite icon (|share-icon|).
    - For Admin:
        #. Filter with Privacy: Private (see :ref:`gallery-admin-search-and-filter`). |permissions|
        #. Click on the invite icon (|share-icon|).

#. You should be sent to a form that allows you to specify emails of the people you want to give access to the private gallery.
#. After entering emails, click on "Invite" to send them email notifications of the granting of access to the private gallery.