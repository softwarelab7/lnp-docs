Giving Access to Admin Modules to Users
=======================================
#. Log in to the site with admin credentials.
#. Go to Manage > Users on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`).
#. Search for the user you want to add admin credentials to.
#. Click on the pencil icon (|pencil-icon|) on the row of the user.
#. Check the boxes on the module permissions you want the user to be given.
#. Submit the form by clicking on the Edit User button.