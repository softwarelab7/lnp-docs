Creating an Account
======================
To create an account please follow these steps:

#. Click on the Register Link on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`).
#. Accomplish form and submit. This should take you to a page with a message of the next step.
#. You should be receiving an email to activate your account from the site.
#. Click on activation link to activate your account so that you can use it to log in to the site (see :doc:`login`).