Navigation Menu
===============
To get to a link on the Navigation Menu, please check the page layout (see :ref:`main-layout`) or if you cannot find it readily, you might be using a smaller screen or have too many menu items given to you (see :ref:`main-hidden-nav-menu-layout`).