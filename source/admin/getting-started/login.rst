Logging in to the Website
=========================

#. Click on the Login Link on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`).
#. Enter email/username and password and submit form.
#. Verify login status by looking for the Logout Link on the Navigation Menu (see :doc:`/admin/getting-started/nav-menu`).