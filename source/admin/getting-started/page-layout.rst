Page Layouts
===================

Before being able to read the docs more proficiently, you must first need to be aware of the terminologies that we use for the page layouts in order to locate things mentioned in this doc faster.

.. _main-layout:

Main Layout
------------
.. image:: /_static/admin/getting-started/page-layout-main.png

.. _main-hidden-nav-menu-layout:

Main Layout with hidden Navigation Menu Items
------------------------------------------------------
If you can't see the link, it could be hidden due to the number of menu items on display at the moment or your screen is too narrow. In this case, you would be able to show the menu item by clicking on More.

.. image:: /_static/admin/getting-started/page-layout-main-hidden-nav-menu-layout.png