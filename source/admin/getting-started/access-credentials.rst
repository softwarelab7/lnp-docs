Getting Access to Admin Modules
=======================================

Prerequisites
-------------
1. Should be able to login to an account. If you haven't already we suggest you check out the section :doc:`index`.
2. Should be able to contact current administrator.

Steps
-------------
1. Contact current site administrator and ask her to give you access to modules you need to use. If administrator does not know how to, please direct administrator to :doc:`/admin/user-management/giving-admin-access`.